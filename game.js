let track = [
    [0, 3],
    [1, 3],
    [2, 3],
    [3, 3],
    [4, 3],
    [5, 2],
    [5, 1],
    [5, 0],
    [5, -1],
    [5, -2],
    [4, -3],
    [3, -3],
    [2, -2],
    [1, -1],
    [0, -1],
    [-1, -1],
    [-2, -1],
    [-3, -2],
    [-4, -3],
    [-5, -2],
    [-5, -1],
    [-5, 0],
    [-5, 1],
    [-5, 2],
    [-4, 3],
    [-3, 3],
    [-2, 3],
    [-1, 3]
];

let questions = [{
        text: "What is 2 x 12?",
        answer: 24
    }
    , {
        text: "When is Mrs Bishop's birthday?",
        choices: ["last week", "today", "she doesn't have birthdays"],
        correctChoice: "today"

    }
];

let player1 = 'player1.png';

let player2 = 'player2.png';

let finished = false;
let currentQuestion;

let autoRunInterval;

$(() => {
    $('.race-track, .race-grid').fadeIn('fast').promise().done(() => {
        $('.start').fadeIn('fast');
    });

    $('.start').click(() => {
        clearInterval(autoRunInterval);
        currentPostition = undefined;
        nextQuestion();
    })

    $('.go').click(() => {
        if (questionIsNumber(currentQuestion)) {
            if (currentQuestion.answer == $('.question.number #answer input').val()) {
                nextQuestion();
            }
        } else if (currentQuestion.correctChoice == $('.question.choice #answer select option:selected').val()) {
            nextQuestion();
        }
    });

    autoRunInterval = setInterval(() => {
        movePlayerCounter();
    }, 250);
});

function nextQuestion() {
    setCurrentQuestion();

    movePlayerCounter();

    $('.question').fadeOut('fast').promise().done(() => {
        if (questionIsNumber(currentQuestion)) {
            $('.question.number #answer input').val('');
            $('.question.number .text').text(currentQuestion.text);
            $('.question.number').fadeIn();
        } else {
            $('.question.choice .text').text(currentQuestion.text);
            $('.question.choice .choices').empty();
            for (let choice of currentQuestion.choices) {
                $('.question.choice .choices').append(`<option val="${choice === currentQuestion.correctChoice ? 'correct' : 'wrong'}">${choice}</option>`);
            }
            $('.question.choice').fadeIn();
        }
    });
}

function setCurrentQuestion() {
    if (!currentQuestion || questions.indexOf(currentQuestion) + 1 === questions.length) {
        currentQuestion = questions[0];
    } else {
        currentQuestion = questions[questions.indexOf(currentQuestion) + 1];
    }
}

let currentPostition;

function movePlayerCounter() {
    let lastPosition = currentPostition;
    if (!currentPostition || track.indexOf(currentPostition) + 1 === track.length) {
        currentPostition = track[0];
    } else {
        currentPostition = track[track.indexOf(currentPostition) + 1];
    }
    let column = currentPostition[0] + 6;
    let row = 9 - (currentPostition[1] + 5);
    $('.race-square.track').empty();
    $($('.race-square.track')[column + (row * 13)]).append('<img class="player1" src="player1.png" />');
    if (lastPosition) {
        setDirection(lastPosition, currentPostition);
    }
}

function questionIsNumber(question) {
    if (typeof question.answer === 'number') {
        return true;
    } else {
        return false;
    }
}

function setDirection(lastPosition, nextPosition) {
    let direction = getDirection(lastPosition, nextPosition);
    $('.player1').css({
        WebkitTransform: 'rotate(' + direction + 'deg)'
    });
}

function getDirection(lastPosition, nextPosition) {
    let theta = Math.atan2(nextPosition[0] - lastPosition[0], nextPosition[1] - lastPosition[1]);
    if (theta < 0.0) theta += (Math.PI * 2);
    return (57.2957795130823209 * theta) - 90;
}